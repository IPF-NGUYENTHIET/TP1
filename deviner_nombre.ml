Random.self_init()
let nb = Random.int (int_of_string (Sys.argv.(1)))

let rec devine ()=
  let guess = read_int() in
  if guess > nb then (Printf.printf "Trop grand\n"; devine () ) else (
    if guess < nb then (Printf.printf "Trop petit\n"; devine () ) else (
      Printf.printf "Bravo\n"
    )
  )

  let rec devine2 n cpt =
    match n with
    | 0 -> Printf.printf "Vous avez perdu\n"
    | _ -> 
      let guess = read_int() in
      let nb_tentative = n-1 in

      if guess = nb then
        (Printf.printf "%s" ("Bravo," ^ string_of_int (cpt) ^ " tentatives\n"))
      else
      (
        if guess > nb then
         (
          Printf.printf "%s" ( "-, il vous reste " ^ string_of_int (nb_tentative) ^ " tentative(s)\n");
           devine2 nb_tentative (cpt+1)
         )
        else
        (
          Printf.printf "%s" ( "+, il vous reste " ^ string_of_int (nb_tentative) ^ " tentative(s)\n");
          devine2 nb_tentative (cpt+1)
        )
      )
let () =
  devine2 15 1
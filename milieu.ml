let x1 = read_int ()
let y1 = read_int ()
let x2 = read_int ()
let y2 = read_int ()

let moy x y =
  ( float_of_int x +. float_of_int y ) /. 2.

let moy_x = moy x1 x2 
let moy_y = moy y1 y2 

 
let longueur =
  let tmp1 = (x1 - x2) in
  let tmp2 = (y1 - y2) in
  sqrt ( float_of_int ( tmp1*tmp1 + tmp2*tmp2 ) )

let () =
  Printf.printf "(%d, %d) (%d, %d) : (%f, %f), %f " x1 y1 x2 y2 moy_x moy_y longueur

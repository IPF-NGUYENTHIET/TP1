let annee_ref = 2016 ;;

let est_bissextile_annee a =
  ( a mod 4= 0 ) && ( a mod 100 != 0 ) || ( a mod 400= 0)

let rec jours_entre_annees a1 a2 acc =
  if a1 = a2 then
    acc
  else (
    if est_bissextile_annee a2 then
      jours_entre_annees a1 (a2-1) (366+acc)
    else
      jours_entre_annees a1 (a2-1) (365+acc)
  )

let jours_a_annee a = 
  if a < annee_ref then
    jours_entre_annees a annee_ref 0
  else
    jours_entre_annees annee_ref a 0

let mois_ref = 12 ;;

let nb_jours mois b =
  let m = [1;3;5;7;8;10;12] in
  let eq_m a =
    a = mois
  in
  if (List.exists eq_m m) then
    31
  else (
    if mois = 2 then (
      if b then (
        29
      )
      else (
        28
      )
    )
    else (
      30
    )
  )

let rec jours_entre_mois m1 m2 b acc =
  if (m1 = m2) then
    acc
  else
    jours_entre_mois m1 (m2 -1) b (acc + ( nb_jours m2 b ) )

let jours_a_mois m b acc =
  if m < mois_ref then (
    acc + jours_entre_mois m mois_ref b 0
  )
  else
  (
    acc + jours_entre_mois mois_ref m b 0
  )

let total_jours d m b acc =
  acc - d + (nb_jours m b)

let ecart acc =
 acc mod 7

let jour_corres_ecart e =
  match e with
  | 0 -> "samedi"
  | 1 -> "vendredi"
  | 2 -> "jeudi"
  | 3 -> "mercredi"
  | 4 -> "mardi"
  | 5 -> "lundi"
  | 6 -> "dimanche"
  | _ -> "bug"

let date_a_jour j m a =
  let b = est_bissextile_annee a in

  let res = jour_corres_ecart (
    ecart (
      total_jours j m b (
        jours_a_mois m b (
          jours_a_annee a
        )
      )
    )
  )
  in
  Printf.printf "%s" res

let rec main () =
  Printf.printf "j:\n";
  let j = read_int () in
  Printf.printf "m:\n";
  let m = read_int () in
  Printf.printf "a:\n";
  let a = read_int () in
  date_a_jour j m a;
  main ()

let () =
  main ()
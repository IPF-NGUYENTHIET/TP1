let left = 0. ;;
let right = 300. ;;
let down = 0.;;
let up = 500.;;

let ball  = 5;;

let draw_ball x y =
  let xi = int_of_float x in
  let yi = int_of_float y in
  Graphics.fill_circle xi yi ball
;;


let init_vec =
  let x =
    Random.float 1. in
  let y =
    Random.float 1. in
  (x,y)
;;

let new_position (x,y) (a,b) =
  (x+.a,y+.b)
;;

let paddle = 100 ;;

let thick = 10 ;;

let draw_paddle n =
  Graphics.fill_rect n (int_of_float down) paddle thick 
;;

let position_paddle () =
  let x = fst ( Graphics.mouse_pos () ) in
  max (int_of_float left) (min ((int_of_float right) - paddle) x);
;;

let bounce (x,y) (a,b) p =
  let a = if (left < x && x < right) then a else (-1.) *. a in
  let b = if ((y > up) || (y < (float_of_int thick) && x < ((float p) +. (float_of_int paddle)) && x > (float_of_int p) )) then (-1.) *. b else b in
  (a,b)
;;

let rec game (x,y) (a,b) =
  Graphics.clear_graph();
  draw_ball x y;
  let pp = position_paddle() in
  draw_paddle pp;
  Graphics.synchronize ();
  if (y<down) then failwith "perdu" else (
    let na,nb = bounce (x,y) (a,b) pp in
    let nx,ny = new_position (x,y) (na,nb) in
    game (nx,ny) (na,nb)
  )
  
let () =
  Random.self_init ();

  Graphics.open_graph ( " " ^
    (string_of_int ( int_of_float right )) ^ "x" ^
    (string_of_int ( int_of_float up) )
  ) ;
  Graphics.auto_synchronize false ;

  game (20.,float thick) (0.01,0.01)
;;